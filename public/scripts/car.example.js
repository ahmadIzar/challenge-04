class Car {
  static list = [];

  static init(cars) {
    this.list = cars.map((i) => new this(i));
  }

  constructor({
    id,
    plate,
    manufacture,
    model,
    image,
    rentPerDay,
    capacity,
    description,
    transmission,
    available,
    type,
    year,
    options,
    specs,
    availableAt,
  }) {
    this.id = id;
    this.plate = plate;
    this.manufacture = manufacture;
    this.model = model;
    this.image = image;
    this.rentPerDay = rentPerDay;
    this.capacity = capacity;
    this.description = description;
    this.transmission = transmission;
    this.available = available;
    this.type = type;
    this.year = year;
    this.options = options;
    this.specs = specs;
    this.availableAt = availableAt;
  }

  render() {
    return `
      <img src="${this.image}" alt="${this.manufacture}" class="image-car">
      <div class="text-description">
        <p clas="item-name">Nama / Tipe Mobil : (${this.manufacture} / ${this.type})</p>
        <p class="price">Rp. ${this.rentPerDay} / hari</p>
        <p class="description">${this.description}</p>
        <div class="capacity-item">
          <i class='bx bx-user'></i>
          <p class="capacity">${this.capacity} orang</p>
        </div>
        <div class="capacity-item">
          <i class='bx bx-user'></i>
          <p class="tranmision">${this.transmission}</p>
        </div>
        <div class="capacity-item">
          <i class='bx bxs-calendar-alt'></i>
          <p class="year">${this.year}</p>
        </div>
      </div>
      <a href="#" class="btn btn-pilih">Pilih Mobil</a>
    `;
  }
}
