class App {
  constructor() {
    this.clearButton = document.getElementById("clear-btn");
    this.loadButton = document.getElementById("load-btn");
    this.carContainerElement = document.getElementById("cars-container");

    this.driver = document.getElementById("driver");
    this.tanggal = document.getElementById("date");
    this.waktu = document.getElementById("time");
    this.penumpang = document.getElementById("pass");
  }

  async init() {
    await this.load();

    // Register click listener
    this.clearButton.onclick = this.clear;
    this.loadButton.onclick = this.run;
  }

  run = () => {
    let d = this.tanggal.value + "T" + this.waktu.value;
    let formdate = Date.parse(d);
    Car.list.forEach((car) => {
      let penumpang = this.penumpang.value;
      let drive = this.driver.value;
      if (drive == "true") {
        drive = true;
      } else drive = false;
      console.log(car.availableAt);

      // let tanggale = new Date(this.tanggal.value);

      let fsw = Date.parse(car.availableAt);

      if (
        car.available == drive &&
        fsw >= formdate &&
        car.capacity >= penumpang
      ) {
        const node = document.createElement("div");
        node.className = "card card-body ";
        node.innerHTML = car.render();
        this.carContainerElement.appendChild(node);
      }
    });
  };

  async load() {
    const cars = await Binar.listCars();
    Car.init(cars);
  }

  clear = () => {
    let child = this.carContainerElement.firstElementChild;

    while (child) {
      child.remove();
      child = this.carContainerElement.firstElementChild;
    }
  };
}
